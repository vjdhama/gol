class Board
	attr_accessor :board, :size, :alive_locations, :upper_bound_x, :upper_bound_y

	def initialize(size)
		@size = size
		@board = []
		@alive_locations = []
		@upper_bound_x = size[0]
		@upper_bound_y = size[1]
		#(1..size[0]).each{|b| @board << Array.new(size[1])}
	end

	def insert_cells(alive_locations)
		@alive_locations = alive_locations
		(0..size[0]-1).each do |a|
			@board << []
			(0..size[1]-1).each do |b|
				if alive_locations.include? [a+1,b+1]
					@board[a] << '*'
				else
					board[a] << '0'
				end
			end
		end
	end

	def draw
		@board.each do |row|
			row.each do |element|
				print element
			end
			puts
		end
	end

	def alive_dies?(location)
		live_neighours = live_neighbours_count(location)
	end

	def dead_lives?(location)

	end

	def evolve
		(0..size[0] - 1).each do |a|
			(0..size[1] - 1).each do |b|
				if @board[a][b] == "*"
					alive_dies?([a,b])
				else
					dead_lives?([a,b])
				end 
			end
		end
	end

	def live_neighbours_count(location)
		count = 0
		neighbours
		if @board[location[0][location[1]]
	end

	def neighbours(cell)
		n = []
		[[0,1],[0,-1],[1,-1],[1,0],[1,1],[-1,-1],[-1,0],[-1,1]].each do |a|
			x = cell[0] - a[0]
			y = cell[1] - a[1]
			if x >= 0 && x < @upper_bound_x && y >= 0 && y < @upper_bound_y
				n << [x,y]
			end
		end
		n
	end
end

gol_board = Board.new([5,5])
alive_locations = [[2,3],[3,2],[1,2],[2,2],[5,1]]
gol_board.insert_cells(alive_locations)

gol_board.draw

(1..5).each do |i|
  gol_board.evolve
  sleep(1)
end
